import { Component, OnInit, ViewEncapsulation, ViewChild, EventEmitter, ElementRef, AfterViewInit, HostListener } from '@angular/core';
import { DynamicScriptLoaderService } from '../../dynamic-script-loader-service.service';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { DBXHttpService } from '../../core/dbx-http/dbx-http.service';
import * as moment from 'moment';
declare const $: any;
declare const Dropzone: any;
import { _ } from 'underscore';
import { NgForm } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { NotificationService } from '../../core/services/notification.service';
import * as Tesseract from 'tesseract.js';
export class CustomPixel {
  x: any;
  y: any;
}

export class CustomRect {
  x0: any;
  y0: any;
  x1: any;
  y1: any;
}


function readBase64(file): Promise<any> {
  var reader  = new FileReader();
  var future = new Promise((resolve, reject) => {
    reader.addEventListener("load", function () {
      resolve(reader.result);
    }, false);

    reader.addEventListener("error", function (event) {
      reject(event);
    }, false);

    reader.readAsDataURL(file);
  });
  return future;
}

@Component({
  selector: 'app-tasksingle',
  templateUrl: './tasksingle.component.html',
  styleUrls: ['./tasksingle.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TasksingleComponent implements OnInit, AfterViewInit {
  subTasklists = [];
  isDetailsShow: any = false;
  singleTask: NgForm;
  @ViewChild('singleTask') ssingleTask: NgForm;
  taskId: string;
  formDataLoaded = false;
  taskDetails: any = [];
  searchTextString = '';
  commentDetails: any = [];
  processingForm: any = [{}];
  involvePeople: any = '';
  involvePeopleList: any = [];
  accountId: any;
  loader = false;
  pdfpage = 1;
  zoompage = 2;
  urldownload: any;
  contentData: any;
  pdfUrl: any;
  seletedItem: any = {};
  usersData: any = [];
  keyword = 'fullName';
  onlyReadableForm = [];
  imageBase64 : any = null;
  pdfLoader = false;
  taskGetParam: any = {
    page: 0,
    state: 'completed',
    assignment: 'involved',
    sort: 'created-desc'};

      // uploader
      uploader: FileUploader;
      response: string;
      public hasBaseDropZoneOver = false;
      public hasAnotherDropZoneOver = false;


    // rubber binding variable
    cvs: HTMLCanvasElement;
    bodyrect;
    canvasrect;
    containerelem;
    img;
    clickedx;
    clickedy;
    pngwidth = 1682;
    pngheight = 2177;
    ocrResult: any;
    xintervals: CustomRect[] = [];
    yintervals: CustomRect[] = [];
    currentRects: CustomRect[] = [];
    iter1Indexes: any[] = [];
    iter2Indexes: any[] = [];
    clickedOn: any = null;
    fieldLabel: any = "";

    webpageload = false;

  // tslint:disable-next-line:max-line-length
  constructor(private dynamicScriptLoader: DynamicScriptLoaderService, public router: Router,  private route: ActivatedRoute, private DBXHttp: DBXHttpService, private http: Http, private notify: NotificationService, private elementRef: ElementRef) {
    this.urldownload = environment.api_endpoint;
  }

  public onFileSelected(event: EventEmitter<File[]>) {
    const file: File = event[0];
    this.uploader.uploadAll();
  }

  ngOnInit() {
    'use strict';
    this.startScript();
    // this.initBasicSelect();
    this.route.params.subscribe( params => {
      this.taskId = params.task_id;
      this.accountId = params.account_id;
      this.taskGetParam = [this.taskId];
      this.fetchSingleTask();
      this.fetchSubTask();
    });

  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
    this.uploader.uploadAll();
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
    this.uploader.uploadAll();
  }

  fetchSingleTask() {
    this.DBXHttp.get('DB-task/app/rest/tasks/' + this.taskGetParam).subscribe((res: any) => {
      this.taskDetails = res;
      if (this.taskDetails.involvedPeople) {
        this.involvePeople = _.pluck(this.taskDetails.involvedPeople, 'fullName').join(', ');
        this.involvePeopleList = this.taskDetails.involvedPeople;
      }
      this.toolTip();
      this.fetchLatestComment();
      this.fetchContent();
      if (this.taskDetails.formKey) {
        this.fetchProcessingFrom();
      }
      this.initUPloader(this.taskDetails.processInstanceId);
    });
  }

  initUPloader(instanceId) {
    // tslint:disable-next-line:max-line-length
    this.uploader = new FileUploader({url: environment.api_endpoint + 'DB-task/app/rest/process-instances/' + instanceId + '/raw-content'});
    this.uploader.onCompleteItem = (item: FileItem, response, status, header) => {
      if (status === 200) {
        this.zoompage = 2;
        this.fetchContent();
      }
    };
  }

  fetchProcessingFrom() {
    this.loader = true;
    this.DBXHttp.get('DB-task/app/rest/task-forms/' + this.taskGetParam).subscribe((res: any) => {
      this.processingForm = res;
      let FormObject = {};
      this.processingForm.fields = this.processingForm.fields.map((item) => {
        if (item.type === 'date') {
          item.value = moment(item.value, 'D-M-YYYY').format('YYYY-MM-DD');
        }

        if (item.id === 'vendoremail') {
          item.type = 'email';
        }

        if (item.value) {
          FormObject[item.id] = item.value;
        }
        if (item.readonly) {
          this.onlyReadableForm.push(item.id);
        }
        return item;
      });
      this.formDataLoaded = true;
      this.removeAttributes(this.processingForm.fields);
      var that = this;
      setTimeout(function() {
        console.log(that.ssingleTask);
        that.ssingleTask.form.patchValue(FormObject);
        that.loader = false;
        that.loadData();
      }, 3000);
    });
  }
  
  fetchContent() {
    this.DBXHttp.get('DB-task/app/rest/process-instances/' + this.taskDetails.processInstanceId + '/content').subscribe((res: any) => {
      this.contentData = res;
      // tslint:disable-next-line:only-arrow-functions
      const pdfData = _.findWhere(this.contentData.data, {simpleType: 'pdf'});
      if (!_.isEmpty(pdfData)) {
        this.pdfUrl = this.urldownload + 'DB-task/app/rest/content/' + pdfData.id + '/raw';
      }
      var that = this;
      console.log(this.pdfUrl);
    });
  }

  deleteContent(contentId) {
    this.DBXHttp.delete('DB-task/app/rest/content/' + contentId).subscribe((res: any) => {
      this.fetchContent();
    });
  }

  removeAttributes(fields) {
    // tslint:disable-next-line:only-arrow-functions
    setTimeout(function() {
      fields.forEach(element => {
        if (!element.readOnly) {
          if (document.getElementById(element.id) !== null) {
            document.getElementById(element.id).removeAttribute('readonly');
          }
        }
      });
    }, 3000);
  }

  fetchLatestComment() {
    // tslint:disable-next-line:max-line-length
    this.DBXHttp.get('DB-task/app/rest/process-instances/' + this.taskDetails.processInstanceId + '/comments', {params: {latestFirst: true}}).subscribe((res: any) => {
      console.log(res);
      this.commentDetails = res;
    });
  }

  updateOpenTask(form: any) {
    console.log(form);
    const formValues = {formId: this.processingForm.id, values: form.value};
    // tslint:disable-next-line:max-line-length
    this.DBXHttp.post('DB-task/app/rest/task-forms/' + this.taskDetails.id + '/save-form', formValues).subscribe((res: any) => {
      this.notify.showSuccess('WorkUnit id saved succesfully');
      this.fetchProcessingFrom();
    }, error => {
      if (error) {
        const errorData = JSON.parse(error);
        this.notify.showError('WorkUnit id could not be saved due to' + errorData.message);
      }
    });
  }

  completeTask(type = null, form: any = null) {
    if (type === 'validation') {
      this.validationPost(form);
    } else if (type === 'duplicate') {
      this.duplicatePost(form);
    } else if (type === 'Match') {
      this.matchPost(form);
    } else if (type === 'complete') {
      this.defaultPost(form, 'complete');
    } else {
      this.defaultPost(form, null);
    }
  }

  matchPost(form: any) {
    const formValues = {formId: this.processingForm.id, outcome: 'Match', values: { taskIdActual: this.taskDetails.id}};
    formValues.values = Object.assign(formValues.values, form.value);
    this.DBXHttp.post('DB-task/app/rest/task-forms/' + this.taskDetails.id, formValues).subscribe((res: any) => {
      this.notify.showSuccess('WorkUnit id submitted succesfully');
      this.fetchSingleTask();
      this.router.navigate(['task/Validation']);
    }, error => {
      if (error) {
        const errorData = JSON.parse(error);
        this.notify.showError('WorkUnit id could not be submitted due to ' +
          errorData.message);
      }
    });
  }

  duplicatePost(form: any) {
    const formValues = {formId: this.processingForm.id, outcome: 'Check Duplicate', values: { taskIdActual: this.taskDetails.id}};
    formValues.values = Object.assign(formValues.values, form.value);
    this.DBXHttp.post('DB-task/app/rest/task-forms/' + this.taskDetails.id, formValues).subscribe((res: any) => {
      this.notify.showSuccess('WorkUnit id submitted succesfully');
      this.fetchSingleTask();
      this.router.navigate(['task/Matching Process']);
    }, error => {
      if (error) {
        const errorData = JSON.parse(error);
        this.notify.showError('WorkUnit id could not be submitted due to ' + errorData.message);
      }
    });
  }

  validationPost(form: any) {
    const formValues = {formId: this.processingForm.id, values: {taskIdActual: this.taskDetails.id, validation: form.value.validation}};
    this.DBXHttp.post('DB-task/app/rest/task-forms/' + this.taskDetails.id, formValues).subscribe((res: any) => {
      this.notify.showSuccess('WorkUnit id submitted succesfully');
      this.fetchSingleTask();
      this.router.navigate(['task/Payment']);
    }, error => {
      if (error) {
        const errorData = JSON.parse(error);
        this.notify.showError('WorkUnit id could not be submitted due to ' + errorData.message);
      }
    });
  }

  defaultPost(form: any, type: any) {
    console.log(type);
    const formValues = {formId: this.processingForm.id, values: { taskIdActual: this.taskDetails.id}};
    formValues.values = Object.assign(formValues.values, form.value);
    this.DBXHttp.post('DB-task/app/rest/task-forms/' + this.taskDetails.id, formValues).subscribe((res: any) => {
      this.notify.showSuccess('WorkUnit id submitted succesfully');
      if (type === 'complete') {
        this.router.navigate(['task/Duplicate  Check']);
      }
      this.fetchSingleTask();
    }, error => {
      if (error) {
        const errorData = JSON.parse(error);
        this.notify.showError('WorkUnit id could not be submitted due to ' + errorData.message);
      }
    });
  }

  submitClaim(taskId) {
    this.DBXHttp.put('DB-task/app/rest/tasks/' + this.taskDetails.id + '/action/claim', {}).subscribe((res: any) => {
      this.notify.showSuccess('WorkUnit has been claim successfully');
      this.fetchSingleTask();
    });
  }

  fetchSubTask() {
    // tslint:disable-next-line:max-line-length
    this.DBXHttp.get('DB-task/app/rest/tasks/' + this.taskGetParam + '/subtasks', {params: {latestFirst: true}}).subscribe((res: any) => {
      console.log(res);
      this.subTasklists = res;
    });
  }

  checkValue(event: any) {
    this.isDetailsShow = event.target.checked;
  }

  async startScript() {
    await this.dynamicScriptLoader.load('form.min', 'lightgallery').then( data => {
      this.loadData();
    }).catch(error => console.log(error));
  }

  private toolTip() {
    $('[data-toggle="tooltip"]').tooltip({container: 'body', html: true});
  }

  private initBasicSelect() {
    /* basic select start*/
    $('select').formSelect();
    $('#sel').formSelect();
    var data = [{ id: 1, name: "Option 1" }, { id: 2, name: "Option 2" }, { id: 3, name: "Option 3" }];

    var Options = "";
    $.each(data, function (i, val) {
      $('#sel').append("<option value='" + val.id + "'>" + val.name + "</option>");
      $('#sel').formSelect();
    });
    /* basic select end*/
  }

  private loadData() {
    var that = this;
    $('#aniimated-thumbnials').lightGallery({
      thumbnail: true,
      selector: 'a'
    });
    $("#form_advanced_validation input").on('click', function() {
      $("#form_advanced_validation input").removeClass('lastClick');
      $(this).addClass("lastClick");
    });
    $('#pdfContainer').resize(function($event) { 
      console.log($event.target.clientWidth);
    
      if(($event.target.clientWidth == 902 || $event.target.clientWidth == 802) && that.webpageload == true) {
        console.log("ttttt");
        //that.pageRendered(true);
      }
      that.webpageload = true;
    });

    // Advanced Form Validation
    $('#form_advanced_validation').validate({
        rules: {
            'date': {
                customdate: true
            },
            'creditcard': {
                creditcard: true
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });
  }

  changeZoomInOut($event) {
    this.zoompage = $event.target.value;
  }

  changeInputPage($event) {
    this.pdfpage = $event.target.value;
  }

   // involve people
   selectEvent(item) { this.seletedItem = item; this.assignUser(); }
   onChangeSearch(val: string) { if (val) { this.searchUser(val); } else { this.usersData = []; } }
   clearInput(e) { this.seletedItem = {}; }

  searchUser(val) {
    // tslint:disable-next-line:max-line-length
    this.DBXHttp.get('DB-task/app/rest/workflow-users?filter=' + val).subscribe((res: any) => {
      this.usersData = res.data;
    });
  }

  assignUser() {
    if (this.seletedItem) {
      // tslint:disable-next-line:max-line-length
      this.DBXHttp.put('DB-task/app/rest/tasks/' + this.taskId + '/action/assign', { assignee: this.seletedItem.id }).subscribe((res: any) => {
        this.fetchSingleTask();
        $('#select_person .cancel').click();
        this.seletedItem = {};
      });
    }
  }


  checkcondition(endDate, assignee) {
    return (endDate !== undefined && endDate === null && assignee);
  }

  pageRendered(resize) {
    // this.zoompage=2;
    var that = this;
    this.cvs = this.elementRef.nativeElement.querySelector('canvas');

    var block = this.cvs.toDataURL('image/png', 1.0).split(";");
    // Get the content type of the image
    var contentType = block[0].split(":")[1];// In this case "image/gif"
    // get the real base64 content of the file
    var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
    console.log("Printing base64 image..")
    console.log(realData);

    // Convert it to a blob to upload
    var blob = this.b64toBlob(realData, contentType, 512);

    this.imageBase64 = blob;
    console.log("cvs", this.imageBase64, this.elementRef, this.cvs.toDataURL('image/png', 1));
    if (this.cvs != null) {
      console.log(this.cvs);
      this.cvs.addEventListener('click', this.getPixel.bind(this));
    }
    this.img = this.elementRef.nativeElement.querySelector("img");
    console.log(this.img);

    this.bodyrect = document.body.getBoundingClientRect();
    console.log(this.bodyrect);
    this.canvasrect = this.cvs.getBoundingClientRect();
    console.log(this.canvasrect);
    this.pdfLoader = true;
    if (this.zoompage > 1) this.doOCR();
   
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    console.log(event);
  }
  ngAfterViewInit() {
    
  }
  

  _listener = (event) => {
    console.log("test");
  }

  getPixel(event) {
    event.stopImmediatePropagation();
    console.log(event);
    // console.log(this.canvasrect);
    this.canvasrect = this.cvs.getBoundingClientRect();
    console.log(this.canvasrect);
    this.clickedx = event.pageX - this.canvasrect.x;
    this.clickedy = event.pageY -window.pageYOffset- (this.canvasrect.y);
    let clickedPixel: CustomPixel = { "x": (this.clickedx), "y": (this.clickedy) }
    console.log("You clicked " + JSON.stringify(clickedPixel));
    // return clickedPixel;
    let originalPixel: CustomPixel = this.clickedPixelToOriginalPixel(clickedPixel);
    console.log("Original Pixel " + JSON.stringify(originalPixel));
    
    this.clickedOn = "test";
   
   
    
    this.currentRects = this.findRect(originalPixel);
    console.log("You clicked inside ")
    //console.log(this.currentRects)
    if (this.iter2Indexes.length > 0) {
      console.log("Recognized word is ");
      console.log("tets");
      console.log(this.ocrResult.words[this.iter2Indexes[0]].text);
      this.clickedOn = this.ocrResult.words[this.iter2Indexes[0]].text;
      let objKey = $('.lastClick').attr('id');
      if(_.isEmpty($('.lastClick').val())) {
        var tempObj = {};
        tempObj[objKey] = this.clickedOn;
        this.ssingleTask.form.patchValue(tempObj);
      }
    }
  }

  clickedPixelToOriginalPixel(clickedPixel: CustomPixel) {
    // let originalPixel: CustomPixel = { "x": "", "y": "" };
    let canvasWidth = (this.canvasrect.width);
    let canvasHeight = (this.canvasrect.height);
    let originalX = this.pngwidth / canvasWidth * clickedPixel.x;
    let originalY = this.pngheight / canvasHeight * clickedPixel.y;
    return { "x": Math.ceil(originalX), "y": Math.ceil(originalY) };
  }

  findRect(pix: CustomPixel) {
    this.yintervals = [];
    this.iter1Indexes = [];
    this.iter2Indexes = [];
    for (let j = 0; j < this.ocrResult.words.length; j++) {
      let tempRect: CustomRect = this.ocrResult.words[j].bbox;
      if (pix.x >= tempRect.x0 && pix.x <= tempRect.x1) {
        this.yintervals.push(tempRect);
        this.iter1Indexes.push(j);
      }
    }
    if (this.yintervals.length > 0) {
      let rectarray: CustomRect[] = [];
      for (let j = 0; j < this.yintervals.length; j++) {
        if (pix.y >= this.yintervals[j].y0 && pix.y <= this.yintervals[j].y1) {
          rectarray.push(this.yintervals[j]);
          this.iter2Indexes.push(this.iter1Indexes[j]);
        }

      }
      if (rectarray.length > 0) return rectarray;
      else return null;
    } else return null;
  }

  doOCR() {
    console.log("Printing image")
    console.log(this.imageBase64);
    console.log("OCR Results");
    var a = this;
    Tesseract.recognize(this.imageBase64, {
      tessedit_pageseg_mode: "6",
    }).progress(function (p) { })
    .then(function (result) {
        console.log('result_text', JSON.stringify(result.text));
        console.log('result ', result);
        console.log(a);
        a.pdfLoader = false;
        a.ocrResult = result;
        a.zoompage=1;
    }, function() {
      a.pdfLoader = false;
    });
    this.pdfLoader = false;
    this.zoompage=1;
    
  }

  b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

}
